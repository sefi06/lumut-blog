﻿# Aplikasi ini dibangun menggunakan Framework Laravel
# Requirement
1. PHP >=7.3
2. Composer
# Panduan Instalasi
1. setelah berhasil melakukan clone, silahkan buat database kosong dengan nama bebas.
2. lalu edit file .env dan sesuaikan konfigurasi database nya :
	DB_DATABASE=blog_lumut
	DB_USERNAME=sefi
	DB_PASSWORD=typepassword
3. melalui terminal / CMD jalankan perintah "composer install"
4. lalu jalankan perintah "php artisan migrate"
5. kemudian jalankan perintah "php artisan db:seed"

# User
1. username	: admin
	password	: admin
2. username	: author
	password	: author
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use Yajra\DataTables\DataTables;
use Cache;
use File;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $datas  = Account::orderBy("username", "desc");

            return Datatables::of($datas)
                    ->addIndexColumn()
                    ->addColumn('action', function($model){
                        return "<a href='/account/edit/$model->username'>Edit</a> - <a href='/account/delete/$model->username'>Delete</a>";
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('admin/account/index');
    }

    public function add()
    {
        return view('admin/account/add');
    }

    public function create(Request $request)
    {
        $data           = new Account();
        $data->username = $request->username;
        $data->name     = $request->name;
        $data->role     = $request->role;
        $data->password = \Hash::make($request->password);

        $data->save();

        return redirect()->route('account');
    }

    public function edit(Request $request)
    {
        $data   = Account::findOrFail($request->id);
        
        return view('admin/account/edit', ['data' => $data]);
    }

    public function update(Request $request)
    {

        $data['username']   = $request->username;
        $data['name']       = $request->name;
        $data['role']       = $request->role;
        if($request->password){
            $data['password']       = \Hash::make($request->password);
        }


        Account::where('username', $request->id)->update($data);

        return redirect()->route('account');
    }

    public function delete(Request $request)
    {
        $data   = Account::findOrFail($request->id);
        
        return view('admin/account/delete', ['data' => $data]);
    }

    public function destroy(Request $request)
    {
        $data   = Account::findOrFail($request->id);
        
        $data->delete();
        
        return redirect()->route('account')->with("success", "Data berhasail dihapus");
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Account;
use Session;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if($request->isMethod('get')){
            if (Auth::check()) {
                return redirect('/dashboard');
            }
            return view('admin/login');
        }
        else {
            $validated  = $request->validate([
                'username'  => 'required',
                'password'  => 'required',
            ]);

            $remember   = $request->remember_me ? true : false;

            if (Auth::attempt($validated, $remember)) {
                $getuser    = Account::where("username", $request->input('username'))->first();
                $request->session()->put('username', $request->input('username'));
                $request->session()->put('name', $getuser->name);
                $request->session()->regenerate();
                return redirect()->intended('/dashboard');
            } else {
                Session::flash('error', 'username atau Password Salah');
                return redirect('/login');
            }
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}

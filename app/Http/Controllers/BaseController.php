<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Yajra\DataTables\DataTables;

class BaseController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $datas  = Post::orderBy("idpost", "desc")->limit(5);

            return Datatables::of($datas)
                    ->addIndexColumn()
                    ->addColumn('action', function($model){
                        return "<a href='/post/edit/$model->idpost'>Edit</a> - <a href='/post/delete/$model->idpost'>Delete</a>";
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }


        return view('admin/home');
    }
}

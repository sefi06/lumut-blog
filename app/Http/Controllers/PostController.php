<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Yajra\DataTables\DataTables;
use Cache;
use File;

class PostController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $datas  = Post::orderBy("idpost", "desc");

            return Datatables::of($datas)
                    ->addIndexColumn()
                    ->addColumn('action', function($model){
                        return "<a href='/post/edit/$model->idpost'>Edit</a> - <a href='/post/delete/$model->idpost'>Delete</a>";
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('admin/post/index');
    }

    public function add()
    {
        return view('admin/post/add');
    }

    public function create(Request $request)
    {
        $data           = new Post();
        $data->title    = $request->title;
        $data->content  = $request->content;
        $data->username = auth()->user()->username;
        $data->date     = date("Y-m-d H:i:s");

        $data->save();

        return redirect()->route('post');
    }

    public function edit(Request $request)
    {
        $data   = Post::findOrFail($request->id);
        
        return view('admin/post/edit', ['data' => $data]);
    }

    public function update(Request $request)
    {

        $data['title']   = $request->title;
        $data['content'] = $request->content;
        $data['username']= auth()->user()->username;
        $data['date']    = date("Y-m-d H:i:s");


        Post::where('idpost', $request->id)->update($data);

        return redirect()->route('post');
    }

    public function delete(Request $request)
    {
        $data   = Post::findOrFail($request->id);
        
        return view('admin/post/delete', ['data' => $data]);
    }

    public function destroy(Request $request)
    {
        $data   = Post::findOrFail($request->id);
        
        $data->delete();
        
        return redirect()->route('post')->with("success", "Data berhasail dihapus");
    }
}

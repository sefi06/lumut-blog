<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;
    protected $table    = "games";
    protected $guarded  = ['id'];

    public function category()
    {
        return $this->belongsTo(Category::class, "category_id", "id");
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Account;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::create(
        [

            'name' => 'admin',
            'username' => 'admin',
            'role' => 'admin',
            'password' => \Hash::make('admin'),

        ],
        [

            'name' => 'author',
            'username' => 'author',
            'role' => 'author',
            'password' => \Hash::make('author'),

        ]
        );
    }
}

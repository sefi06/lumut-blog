@extends('admin.layout.base')

@section('content')

<div class="card">
    <div class="card-header">Tambah Akun</div>
    <div class="card-body">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="username">username</label>
                <input class="form-control" id="username" type="text" name="username" required autocomplete="off">
            </div>
            <div class="mb-3">
                <label for="name">name</label>
                <input class="form-control" id="name" type="text" name="name" required autocomplete="off">
            </div>
            <div class="mb-3">
                <label for="password">password</label>
                <input class="form-control" id="password" type="text" name="password" required autocomplete="off">
            </div>
            <div class="mb-3">
                <label for="role">role</label>
                <div class="form">
                <select name="role" class="form-select" aria-label="Default select example">
                    <option value="admin">Admin</option>
                    <option value="author">author</option>
                </select>
                </div>
            </div>
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                <a class="btn btn-sm btn-danger" href="/post/">Batal</a>
            </div>
        </form>
    </div>
</div>

@endsection
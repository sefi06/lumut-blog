@extends('admin.layout.base')
@section('css')
<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Akun</h1>
    <a href="/account/add" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
        Tambah Data
    </a>
</div>

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        var i = 1;
        $('table').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: "{{ route('account') }}",
            columns: [

                { "data": "username" },
                { "data": "name" },
                { "data": "role" },

                { "data": "action" },

            ],

        });
    });
</script>
@endsection
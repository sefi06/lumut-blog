@extends('admin.layout.base')

@section('content')

<div class="card">
    <div class="card-header">Tambah Postingan</div>
    <div class="card-body">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="title">title</label>
                <input class="form-control" id="title" type="text" name="title" required autocomplete="off">
            </div>
            <div class="form-group">
                <label >Content</label>
                <textarea class="form-control" name="content" rows="3"></textarea>
            </div>
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                <a class="btn btn-sm btn-danger" href="/post/">Batal</a>
            </div>
        </form>
    </div>
</div>

@endsection
@extends('admin.layout.base')

@section('content')

@if(session()->has('msg-exist-reward'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('msg-exist-reward') }}
</div>
@endif

<div class="card">
    <div class="card-header">Delete Hadiah</div>
    <div class="card-body">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="reward">Apakah anda ingin menghapus data ini ?</label>
            </div>
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" type="submit">Ya</button>
                <a class="btn btn-sm btn-danger" href="/post/">Batal</a>
            </div>
        </form>
    </div>
</div>

@endsection
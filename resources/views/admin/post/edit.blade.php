@extends('admin.layout.base')

@section('content')

{{--@if(session()->has('msg-exist-post'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('msg-exist-post') }}
</div>
@endif--}}

<div class="card">
    <div class="card-header">Edit Postingan</div>
    <div class="card-body">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="name">Title</label>
                <input class="form-control" id="title" type="text" name="title" value="{{ $data->title }}" required autocomplete="off">
            </div>
            <div class="form-group">
                <label >Content</label>
                <textarea class="form-control" name="content" rows="3">{{ $data->content }}</textarea>
            </div>
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" type="submit">Simpan</button>
                <a class="btn btn-sm btn-danger" href="/post">Batal</a>
            </div>
        </form>
    </div>
</div>

@endsection
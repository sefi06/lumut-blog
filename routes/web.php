<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProfileController;
use App\Http\Middleware\Admin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::match(['get','post'], '/login', [AuthController::class, 'login'])->name('login');
Route::match(['get','post'], '/', [AuthController::class, 'login'])->name('login');
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [BaseController::class, 'index'])->name('home');
    Route::post('logout', [AuthController::class, 'logout']);
    Route::group(['prefix' => 'post'], function(){
        Route::get('/', [PostController::class, 'index'])->name('post');
        Route::get('add', [PostController::class, 'add'])->name('add_post');
        Route::post('add', [PostController::class, 'create'])->name('create_post');
        Route::get('edit/{id}', [PostController::class, 'edit'])->name('edit_post');
        Route::post('edit/{id}', [PostController::class, 'update'])->name('update_post');
        Route::get('delete/{id}', [PostController::class, 'delete'])->name('delete_post');
        Route::post('delete/{id}', [PostController::class, 'destroy'])->name('destroy_post');
    });

    Route::group(['prefix' => 'account', 'middleware' => 'admin'], function(){
        Route::get('/', [AccountController::class, 'index'])->name('account');
        Route::get('add', [AccountController::class, 'add'])->name('add_account');
        Route::post('add', [AccountController::class, 'create'])->name('create_account');
        Route::get('edit/{id}', [AccountController::class, 'edit'])->name('edit_account');
        Route::post('edit/{id}', [AccountController::class, 'update'])->name('update_account');
        Route::get('delete/{id}', [AccountController::class, 'delete'])->name('delete_account');
        Route::post('delete/{id}', [AccountController::class, 'destroy'])->name('destroy_account');
    });
});
